<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modification d'un animal</title>
</head>
<body>
<nav>
		<a href="<c:url value='/'/>">Accueil</a>
	</nav>
	<br>
	<br>
	<br>
<form:form servletRelativeAction="/modification" method="POST" modelAttribute="animalDto">
		<label> Espèce
			<form:select path="espece">
				<form:options items="${especes}" value="${animal.espece}" itemLabel="nom" itemValue="nom"/>
			</form:select>
		</label>
		<label>Régime
			<form:select path="regime">
				<c:forEach items="${regimes}" var="regime">
					<option><c:out value="${regime}"/></option>
				</c:forEach>	
			</form:select>
		</label>
		<form:input path="origine" value="${animal.origine}"/>
		<form:hidden path="id" value="${animal.id }"/>
		<form:input path="nom" value="${animal.nom}"/> <button type="submit">modifier</button>
		<form:errors path="nom"/>
	</form:form>

</body>
</html>