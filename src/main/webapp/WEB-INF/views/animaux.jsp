<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Animoz - Liste des animaux</title>
</head>
<body>
	<nav>
		<a href="<c:url value='/'/>">Accueil</a>
	</nav>
		<br>
	<br>
	<br>
	<ul>
		<c:forEach items="${animaux}" var="animal">
			<li><a href="<c:url value='/animal/${animal.id}'/>"><c:out value="${animal.nom}"/></a>&nbsp; | <a href="<c:url value='/deleteAnimal/${animal.id}'/>">supprimer</a>
			| <a  href="<c:url value='/updateAnimal/${animal.id}'/>">modifier</a>
			</li>
		</c:forEach>
			
	</ul>
	
		<a href="./ajoutAnimal">Ajouter</a>
</body>
</html>