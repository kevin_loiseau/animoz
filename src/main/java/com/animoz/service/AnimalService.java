package com.animoz.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.controleur.AnimalDto;
import com.animoz.dao.AnimalDao;
import com.animoz.modele.Animal;
import com.animoz.modele.Espece;

@Service
public class AnimalService {
	
	@Autowired
	private AnimalDao animalDao;

	public List<Animal> getAnimaux() {
		return animalDao.getAnimaux();
	}

	public Animal getAnimal(long animalId) {
		return animalDao.getAnimal(animalId);
	}
	
	@Transactional
	public boolean ajouter(AnimalDto animalDto, String espNom) {
		boolean success = false;
		Espece espece = convertStrEspeceToOjb(espNom);
		if(! animalDao.existe(animalDto.getNom())) {
			Animal animal = new Animal();
			animal.setNom(animalDto.getNom());
			animal.setOrigine(animalDto.getOrigine());
			animal.setRegime(animalDto.getRegime());
			animal.setEspece(espece);
			if(animalDao.ajouter(animal)) {
				success = true;
			}
		}
		
		return success;
	}
	
	@Transactional
	public boolean modifier(AnimalDto animalDto, Espece espece) {
		boolean success = false;
		Animal animal = new Animal();
		animal.setNom(animalDto.getNom());
		animal.setOrigine(animalDto.getOrigine());
		animal.setRegime(animalDto.getRegime());
		animal.setEspece(espece);
		if(animalDao.modifier(animal)) {
			success = true;
		}
		
		return success;
	}
	
	public void supprimer(long id) {
		if(animalDao.existeById(id)) {
			Animal animal = new Animal();
			animal.setId(id);
			animalDao.deleteAnimal(animal);
		}
	}
	
	public Espece convertStrEspeceToOjb(String nom) {
		Espece espece = new Espece();
		espece.setNom(nom);
		return espece;
	}

}
