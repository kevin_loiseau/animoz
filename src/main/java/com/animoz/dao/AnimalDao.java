package com.animoz.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.animoz.modele.Animal;

@Repository
public class AnimalDao {
	
	@PersistenceContext
	private EntityManager em;

	public Animal getAnimal(long animalId) {
		return em.find(Animal.class, animalId);
	}

	public List<Animal> getAnimaux() {
		return em.createQuery("select a from Animal a order by a.nom", Animal.class).getResultList();
	}
	
	public boolean existe(String nomAnimal) {
		long nb = em.createQuery("select count(e) from Animal e where lower(e.nom) = lower(:nom)", Long.class)
				    .setParameter("nom", nomAnimal)
				    .getSingleResult();
		return nb > 0;
	}
	
	public boolean existeById(long id) {
		boolean success;
		long nb = em.createQuery("select count(e) from Animal e where lower(e.id) = lower(:id)", Long.class)
			    .setParameter("id", id)
			    .getSingleResult();
		success = nb != 0 ? false : true;
		return success;
	}
	
	@Transactional 
	public boolean ajouter(Animal animal) {

			em.persist(animal);
			return true;
		
	}
	
	@Transactional
	public boolean modifier(Animal animal) {
		em.merge(animal);
		return true;
	}
	
	public Animal get(String nom) {
		List<Animal> animals = em.createQuery("select a from Animal a where a.nom = :nom", Animal.class)
				                 .setParameter("nom", nom)
				                 .getResultList();
		if (animals.size() == 1) {
			return animals.get(0);
		} else {
			return null;
		}
	}
	
	@Transactional 
	public void deleteAnimal(Animal animal) {
		long id = animal.getId();
		em.createQuery("delete from Animal a where a.id = :id").setParameter("id", animal.getId()).executeUpdate();
	}

}
