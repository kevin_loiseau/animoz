package com.animoz.controleur;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.animoz.dao.AnimalDao;
import com.animoz.dao.EspeceDao;
import com.animoz.modele.Espece;
import com.animoz.modele.Regime;
import com.animoz.service.AnimalService;
import com.animoz.service.EspeceService;

@Controller
public class AnimalControleur {
	
	@Autowired
	private AnimalService animalService;
	@Autowired
	private EspeceService especeService;
	
	@GetMapping("/animal")
	public String getListeAnimaux(Model model, @ModelAttribute AnimalDto animalDto) {
		model.addAttribute("animaux", animalService.getAnimaux());
		return "animaux";
	}
	
	@PostMapping("/ajout")
	public String ajouterAnimal(Model model, @Valid @ModelAttribute AnimalDto animalDto, BindingResult binding) {
		if(animalService.ajouter(animalDto, (String) binding.getFieldValue("espece"))) {
			model.addAttribute("message", "L'ajout a réussi");
			return "/message";
		}else {
			model.addAttribute("message", "L'ajout a échoué");
			return "/message";
		}
	}

	@GetMapping("/animal/{animalId}")
	public String getAnimal(Model model, @PathVariable long animalId) {
		model.addAttribute("animal", animalService.getAnimal(animalId));
		return "animal";
	}
	
	@GetMapping("/ajoutAnimal")
	public String ajoutAnimal(Model model, @ModelAttribute AnimalDto animalDto) {
		model.addAttribute("especes", especeService .getEspeces());
		model.addAttribute("regimes", Regime.values());
		return "ajoutAnimal";
	}
	
	@GetMapping("/deleteAnimal/{animalId}")
	public String supprAnimal(Model model, @PathVariable long animalId) {
		animalService.supprimer(animalId);
		model.addAttribute("message", "La suppression réussie");
		return "/message";
	}
	
	@GetMapping("/updateAnimal/{animalId}")
	public String updateAnimal(Model model, @PathVariable long animalId, AnimalDto animalDto) {
		model.addAttribute("animal", animalService.getAnimal(animalId));
		model.addAttribute("especes", especeService .getEspeces());
		model.addAttribute("regimes", Regime.values());
		return "modificationAnimal";
	}
	
	@PostMapping("/modification")
	public String updateAnimal(Model model, @Valid @ModelAttribute AnimalDto animalDto, BindingResult binding) {
		String animalIdStr = (String) binding.getFieldValue("id");
		long animalId = Long.parseLong(animalIdStr);
		System.out.println(animalId);
		System.out.println((String) binding.getFieldValue("espece"));
		AnimalDao animalDao = new AnimalDao();
		if(!animalDao.existeById(animalId)) {
			model.addAttribute("message", "Impossible de modifier cet animal car il n'est pas enregistré");
		}else {
			if(animalService.modifier(animalDto, (Espece) binding.getFieldValue("espece"))) {
				model.addAttribute("message", "Modification réussie");
			}else {
				model.addAttribute("message", "La modification a échoué");
			}
		}
		return "/message";
	}
}
