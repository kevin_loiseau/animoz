package com.animoz.controleur;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotBlank;

import com.animoz.modele.Espece;
import com.animoz.modele.Regime;

public class AnimalDto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = "Vous devez fournir un nom pour l''animal !")
	private String nom;
	@ManyToOne
	private Espece espece;
	private String origine;
	@Enumerated(EnumType.STRING)
	private Regime regime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Espece getEspece() {
		return this.espece;
	}
	
	public void setespece(Espece espece) {
		this.espece = espece;
	}
	
	public String getOrigine() {
		return origine;
	}

	public void setOrigine(String origine) {
		this.origine = origine;
	}
	
	public Regime getRegime() {
		return regime;
	}

	public void setRegime(Regime regime) {
		this.regime = regime;
	}
}
