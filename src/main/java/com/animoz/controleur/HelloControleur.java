package com.animoz.controleur;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloControleur {
	
	@GetMapping(path ="hello")
	public String hello(Model model) {
		System.out.println("hello");
		model.addAttribute("monAttribut",2);
		return "hello";
	}

}


/*
// @ModelAttribute
//@RequestParam

*/